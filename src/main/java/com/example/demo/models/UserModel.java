package com.example.demo.models;

//Librería para mapear las entidades a la base de datos
import javax.persistence.*;

//Etiquetas para que se reconozca la clase como una entidad
@Entity
//Etiqueta para agregar el nombre que la tabla tendrá en la base de datos
@Table(name = "User")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long UserID;
    private String Name;
    private String Email;
    private Integer Priority;

    public Long getUserID() {
        return UserID;
    }

    public void setUserID(Long userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Integer getPriority() {
        return Priority;
    }

    public void setPriority(Integer priority) {
        Priority = priority;
    }
}
